<?php

namespace App\Helpers;
use Config;

class Helper
{
    public static function getColorByName($name)
    {
        $colors = [
            'A' => 'Red',
            'B' => 'Orange',
            'C' => 'Yellow',
            'D' => 'Green',
            'E' => 'Blue',
            'F' => 'Indigo',
            'G' => 'Violet',
            'H' => 'Pink',
            'I' => 'Purple',
            'J' => 'Turquoise',
            'K' => 'Gold',
            'L' => 'Lime',
            'M' => 'Maroon',
            'N' => 'Navy',
            'O' => 'Coral',
            'P' => 'Teal',
            'Q' => 'Brown',
            'R' => 'slategrey',
            'S' => 'Black',
            'T' => 'Sky',
            'U' => 'Berry',
            'V' => 'Grey',
            'W' => 'Straw',
            'X' => 'Silver',
            'Y' => 'seagreen',
            'Z' => 'yellowgreen',
        ];
        if (array_key_exists($name, $colors) && array_key_exists($name, $colors)) {
            return $colors[$name];
        }

        return $color = '#000';
    }
    public static function getDateTimeStamp()
    {
        $date = date_default_timezone_set(Config::get('app.timezone')).date('mdYhis'); 
        if($date !='' && !empty($date)){
            return $date;
        }
        return date_default_timezone_set('Australia/Melbourne').date('mdYhis');
    }
    public static function clean($string) {
       $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
    public static function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
        return request()->ip(); // it will return server ip when no client ip found
    }
}

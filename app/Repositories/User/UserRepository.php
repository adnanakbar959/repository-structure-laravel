<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\Repository;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Traits\CacheableRepository;

class UserRepository extends Repository
{
    use CacheableRepository;

    public function __construct(App $app)
    {
        parent::__construct($app);
    }
    public function model()
    {
        return User::class;
    }
    public function createUser($data)
    {
        $user_data = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),  
        ];
        $user = $this->model->create($user_data);
        return $user;
    }
}

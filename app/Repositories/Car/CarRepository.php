<?php

namespace App\Repositories\Car;

use App\Models\Car;
use App\Repositories\Repository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Container\Container as App;
use Prettus\Repository\Traits\CacheableRepository;

class CarRepository extends Repository
{
    use CacheableRepository;

    public function __construct(App $app)
    {
        parent::__construct($app);
    }
    public function model()
    {
        return Car::class;
    }
    public function createCar($data)
    {
        $car_data = [
            'user_id' => Auth::id(),
            'brand' => $data['brand'],
            'model' => $data['model'],  
        ];
        $car = $this->model->create($car_data);
        return $car;
    }
}

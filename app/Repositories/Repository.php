<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

abstract class Repository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * Retrieve all data of repository, paginated
     *
     * @param  null|int  $limit
     * @param  array  $columns
     * @param  string  $method
     * @return mixed
     */
    public function getAll(
        array $columns = [],
        array $where = [],
        array $like = [],
        array $group = [],
        array $order = ['id' => 'asc'],
        $limit = null

        ) {
        $this->applyCriteria();
        $this->applyScope();
        if (! empty($where)) {
            $this->applyConditions($where);
        }
        if (! empty($like)) {
            $this->applyLike($like);
        }
        if (! empty($group)) {
            $this->applyGroup($group);
        }
        if (! empty($order)) {
            $this->applyOrder($order);
        }
        if (! empty($limit)) {
            $this->applyLimit($limit);
        }
        $columns = empty($columns) ? ['*'] : $columns;
        $results = $this->model->get();
        $this->resetModel();

        return $this->parserResult($results);
    }
    public function getWhereAll(
        array $where = [],
        array $whereNot = [],
        array $whereNull = [],
        array $columns = [],
        array $order = ['id' => 'asc'],
        $limit = null

        ) {
        $this->applyCriteria();
        $this->applyScope();
        if (! empty($where)) {
            $this->applyConditions($where);
        }
        if (! empty($whereNot)) {
            $this->applyWhereNot($whereNot);
        }
        if (! empty($whereNull)) {
            $this->applyWhereNull($whereNull);
        }
        if (! empty($order)) {
            $this->applyOrder($order);
        }
        if (! empty($limit)) {
            $this->applyLimit($limit);
        }
        $columns = empty($columns) ? ['*'] : $columns;
        $results = $this->model->where('status', 1)
        ->get();
        $this->resetModel();

        return $this->parserResult($results);
    }
    public function getCustomWhereLimit(
        array $where = [],
        array $whereNot = [],
        array $whereNull = [],
        array $like = [],
        array $columns = [],
        array $order = ['id' => 'asc'],
        $limit = null
        
        ) {
        $this->applyCriteria();
        $this->applyScope();
        if (! empty($where)) {
            $this->applyConditions($where);
        }
        if (! empty($whereNot)) {
            $this->applyWhereNot($whereNot);
        }
        if (! empty($whereNull)) {
            $this->applyWhereNull($whereNull);
        }
        if (! empty($like)) {
            $this->applyLike($like);
        }
        if (! empty($order)) {
            $this->applyOrder($order);
        }
        if (! empty($limit)) {
            $this->applyLimit($limit);
        }
        $columns = empty($columns) ? ['*'] : $columns;
        $results = $this->model->where('status', 1)
        ->get();
        $this->resetModel();

        return $this->parserResult($results);
    }
    public function paginateBy(
        $limit = null,
         array $columns = [],
         array $where = [],
         array $like = [],
         array $group = [],
         array $order = ['id' => 'asc'],
         array $whereNot = [],
         ) {
        $this->applyCriteria();
        $this->applyScope();
        if (! empty($where)) {
            $this->applyConditions($where);
        }
        if (! empty($like)) {
            $this->applyLike($like);
        }
        if (! empty($group)) {
            $this->applyGroup($group);
        }
        if (! empty($order)) {
            $this->applyOrder($order);
        }
        if (! empty($whereNot)) {
            $this->applyWhereNot($whereNot);
        }
        $limit = is_null($limit) ? 15 : $limit;
        $columns = empty($columns) ? ['*'] : $columns;
        $results = $this->model
        // $results = $this->model->where('status', 1)
        ->{'paginate'}($limit, $columns);
        $results->appends(app('request')->query());
        $this->resetModel();

        return $this->parserResult($results);
    }

    /**
     * Find data by field and value
     *
     * @param  string  $field
     * @param  string  $value
     * @param  array  $columns
     * @return mixed
     */
    public function pluckWhere(array $where, $columns = ['*'])
    {
        $model = $this->findWhere($where, $columns);

        return $model->pluck($columns);
    }
    public function findOneByField($field, $value = null, $columns = ['*'])
    {
        $model = $this->findByField($field, $value, $columns = ['*']);

        return $model->first();
    }
    public function findByFieldFirst(
        array $where,
        $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        if (! empty($where)) {
            $this->applyConditions($where);
        }
        $model = $this->model;

        $this->resetModel();

        return $this->parserResult($model);
    }
    public function findByFieldFirstWherNull(
        array $where =[],
        $whereNull = [],
        $whereNot = [],
        $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();

        // if ($where) {
        //     $this->applyConditions($where);
        // }
        // // if (! empty($whereNot)) {
        // //     $this->applyWhereNot($whereNot);
        // // }
        // // if (! empty($whereNull)) {
        // //     $this->applyWhereNull($whereNull);
        // // }
        // $model = $this->model->findWhere($columns);
        // return $model->first();

        // // $this->resetModel();

        // return $this->parserResult($model);
        $model = $this->model;
        foreach ($where as $field => $value) {
            $model = $model->where($field,'=', $value);
        }
        $result =$model->first($columns);
        $this->resetModel();

        return $this->parserResult($result);
        // return;
    }
    /**
     * Find data by field and value
     *
     * @param  string  $field
     * @param  string  $value
     * @param  array  $columns
     * @return mixed
     */
    public function findOneWhere(array $where, $columns = ['*'])
    {
        $model = $this->findWhere($where, $columns);

        return $model->first();
    }
    public function findOneWhereNot(array $where, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->whereColumn($where)->first();
        $this->resetModel();

        return $this->parserResult($model);

    }
    /**
     * Find data by id
     *
     * @param  int  $id
     * @param  array  $columns
     * @return mixed
     */
    public function find($id, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->find($id, $columns);
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Find data by id
     *
     * @param  int  $id
     * @param  array  $columns
     * @return mixed
     */
    public function findOrFail($id, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->findOrFail($id, $columns);
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Count results of repository
     *
     * @param  array  $where
     * @param  string  $columns
     * @return int
     */
    public function count(array $where = [], $columns = '*')
    {
        $this->applyCriteria();
        $this->applyScope();

        if ($where) {
            $this->applyConditions($where);
        }

        $result = $this->model->count($columns);
        $this->resetModel();
        $this->resetScope();

        return $result;
    }

    /**
     * @param  string  $columns
     * @return mixed
     */
    public function sum($columns)
    {
        $this->applyCriteria();
        $this->applyScope();

        $sum = $this->model->sum($columns);
        $this->resetModel();

        return $sum;
    }

    /**
     * @param  string  $columns
     * @return mixed
     */
    public function avg($columns)
    {
        $this->applyCriteria();
        $this->applyScope();

        $avg = $this->model->avg($columns);
        $this->resetModel();

        return $avg;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    protected function applyLike(array $like)
    {
        foreach ($like as $field => $value) {
            $this->model = $this->model->where($field, 'LIKE', '%'.$value.'%');
        }
    }

    protected function applyGroup(array $group)
    {
        foreach ($group as $value) {
            $this->model = $this->model->groupBy($value);
        }
    }

    protected function applyOrder(array $order)
    {
        foreach ($order as $field => $value) {
            $this->model = $this->model->orderBy($field, $value);
        }
    }

    protected function applyLimit($limit)
    {
        $this->model = $this->model->limit($limit);
    }
    protected function applyWhereNull(array $whereNull)
    {
        foreach ($whereNull as $value) {
            $this->model = $this->model->whereNull($value);
        }
    }
    protected function applyWhereNot(array $whereNull)
    {
        foreach ($whereNull as $field => $value) {
            $this->model = $this->model->where($field ,'!=',$value);
        }
    }
}

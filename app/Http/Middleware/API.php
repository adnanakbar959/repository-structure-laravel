<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class API
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, PATCH, DELETE');
        $response->headers->set('Access-Control-Allow-Headers', 'X-XSRF-TOKEN', 'Origin, Content-Type,CSRF-TOKEN, XSRF-TOKEN,X-CSRF-TOKE, Content-Range , Accept, Authorization, X-Requested-With, Application ,Content-Disposition, Content-Description, X-Auth-Token');
        $response->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $response->headers->set('Pragma', 'no-cache'); //HTTP 1.0
        $response->headers->set('Expires', 'Sat, 01 Jan 1990 00:00:00 GMT'); // // Date in the past

        return $response;
    }
}

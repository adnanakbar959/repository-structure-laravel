<?php

namespace App\Http\Controllers\Api\V1\User;

use JWTAuth;
use JWTAuthException;
use App\Traits\ResponseAPI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\User\LoginRequest;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\User\ProfileRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\User\RegisterRequest;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class UserController extends Controller
{
    use ResponseAPI;

    private $userRepository;

    protected $guard;

    public $token = true;

    protected $base_url;

    public function __construct(UrlGenerator $url,
        UserRepository $userRepository,
    ) {
        $this->middleware('auth:api', ['except' => [
            'login',
            'logout',
            'register',
        ]]);
        $this->guard = request()->has('token') ? 'api' : 'user';
        $this->userRepository = $userRepository;

        auth()->setDefaultDriver($this->guard);
        $this->base_url = $url->to('/');
    }
    public function register(RegisterRequest $request)
    {
        try {
            if (isset($request->validator) && $request->validator->fails()) {
                return $this->error($request->validator->messages(), 200);
            }
            DB::beginTransaction();
            $data = $request->all();
            $user = $this->userRepository->createUser($data);
            DB::commit();
            return $this->success(trans('app.general.signup_successfully'), $user);
        } catch (Exception $e) {
            DB::rollback();
            return $this->error($e->getMessage(), 200);
        }
    }
    private function getToken($email, $password)
    {
        $token = null;
        try {
            if (!$token = JWTAuth::attempt(['email' => $email, 'password' => $password])) {
                return response()->json([
                    'success' => false,
                    'message' => 'Password or email is invalid',
                    'token' => $token,
                ], 401);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'success' => false,
                'message' => 'Token creation failed',
            ], 401);
        }

        return $token;
    }
    public function login(LoginRequest $request)
    {
        try {
            if (isset($request->validator) && $request->validator->fails()) {
                return $this->error($request->validator->messages(), 200);
            }
            $user = $this->userRepository->findOneWhere(['email' => $request->email]);

            if (!$user) {
                return $this->error(trans('app.general.credentials_incorrect'), 200);
            }
            if (!Hash::check($request->password, $user->password)) {
                return $this->error(trans('app.general.credentials_incorrect'), 200);
            }
            if (!$token = JWTAuth::attempt(['email' => $request->email, 'password' => $request->password])) {
                return $this->error(trans('app.general.credentials_incorrect'), 200);
            }
            $token = self::getToken($request->email, $request->password);
            $user->save();
            $user_data = [
                'access_token' => $token,
                'id' => auth()->guard('user')->user()->id,
                'first_name' => auth()->guard('user')->user()->first_name,
                'last_name' => auth()->guard('user')->user()->last_name,
                'email' => auth()->guard('user')->user()->email,
            ];
            return $this->success(trans('app.general.signin_successfully'), $user_data);
        } catch (Exception $e) {
            return $this->error($e->getMessage(), 200);
        }
    }
    public function logout(Request $request)
    {
        try {
            $token = $request->header('Authorization');
            JWTAuth::invalidate(JWTAuth::getToken());
            auth()->guard($this->guard)->logout();
            return $this->success(trans('app.general.signout_successfully'), 200);
        } catch (TokenExpiredException $exception) {
            return $this->error(trans('app.general.token_expire'), 200);
        } catch (TokenInvalidException $exception) {
            return $this->error(trans('app.general.invalid_token'), 200);
        } catch (JWTException $exception) {
            return $this->error(trans('app.general.token_missing'), 200);
        }
    }
    public function updateProfile(ProfileRequest $request)
    {
        try {
            if (isset($request->validator) && $request->validator->fails()) {
                return $this->error($request->validator->messages(), 200);
            }
            $user = auth()->user();
            if (isset($user) && !empty($user)) {
                $user = $this->userRepository->findOneWhere(['id' => $user->id]);
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->update();
                return $this->success(trans('app.general.update_data_successfully'), $user);
            }
            return $this->error(trans('app.general.invalid_user'), 200);
        } catch (Exception $e) {
            return $this->error($e->getMessage(), 400);
        }
    }
}

<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Traits\ResponseAPI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Routing\UrlGenerator;
use App\Http\Requests\Car\CarRequest;
use App\Repositories\Car\CarRepository;
use Illuminate\Support\Facades\Validator;

class CarController extends Controller
{
    use ResponseAPI;
    protected $guard;

    protected $base_url;
    public function __construct(UrlGenerator $url,CarRepository $carRepository)
    {
        $this->carRepository = $carRepository;
        $this->base_url = $url->to('/');
    }

    public function index(Request $request)
    {
        try {
            $user = auth()->user();
            $car = $this->carRepository->getAll(
                [],
                [
                'user_id'=> $user->id,
                ],
                [], 
                [], ['id' => 'desc']);
                return $this->success(trans('app.general.get_data_successfully'), $car);
        } catch (Exception $e) {
            return $this->error($e->getMessage(), 200);
        }
    }
    public function store(CarRequest $request)
    {
        try {
            if (isset($request->validator) && $request->validator->fails()) {
                return $this->error($request->validator->messages(), 200);
            }
            DB::beginTransaction();
            $data = $request->all();
            $car = $this->carRepository->createCar($data,null);
            DB::commit();
            return $this->success(trans('app.general.add_data_successfully'), $car);
        } catch (Exception $e) {
            DB::rollback();
            return $this->error($e->getMessage(), 200);
        }
    }
    public function update(CarRequest $request)
    {
        try {
            if (isset($request->validator) && $request->validator->fails()) {
                return $this->error($request->validator->messages(), 200);
            }
            $car = $this->carRepository->findOneWhere(['id'=>$request->car_id]);
            if (isset($car) && !empty($car)) {
                $car->brand  = $request->brand;
                $car->model  = $request->model;
                $car->update();
                return $this->success(trans('app.general.update_data_successfully'), $car);
            }
            return $this->error(trans('app.general.not_found'), 200);
        } catch (Exception $e) {
            return $this->error($e->getMessage(), 200);
        }
    }
    public function destroy(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id' => 'required|string|exists:cars,id',
            ]);
            if (isset($validator) && $validator->fails()) {
                return $this->error($validator->messages(), 200);
            }
            $car =  $this->carRepository->findOneWhere(['id'=>$request->id]);
            if (! empty($car) && isset($car)) {
                $car->delete();
                return $this->success(trans('app.general.delete_data_successfully'), '');
            } 
            return $this->error(trans('app.general.not_found'), 200);
        } catch (Exception $e) {
            return $this->error($e->getMessage(), 200);
        }
    }
}

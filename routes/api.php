<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\Car\CarController;
use App\Http\Controllers\Api\V1\User\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group([
    'middleware' => ['api'],
    'prefix' => 'v1/auth',
], function ($router) {
    Route::controller(UserController::class)->group(function () {
        Route::post('/register', 'register');
        Route::post('/login', 'login');
    });
    Route::group(['middleware' => ['jwt-auth', 'auth.jwt', 'jwt.auth']], function () {
        Route::controller(UserController::class)->group(function () {
            Route::post('/logout', 'logout');
            Route::post('/update-profile', 'updateProfile');
        });
        Route::controller(CarController::class)->group(function () {
            Route::get('/retrieve', 'index');
            Route::post('/store', 'store');
            Route::post('/update-car', 'update');
            Route::post('/delete', 'destroy');
        });
    });
});
